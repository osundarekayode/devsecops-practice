import requests
import sys

file_name = sys.argv[1]
scan_type = ''

if file_name == 'giffLeaks.json':
    scan_type = 'Gitleaks Scan'
elif file_name == 'njsscan.serif':
    scan_type = 'SARIF'
elif file_name == 'semgrep. json':
    scan_type = 'Semgrep JSON Report'
elif file_name == 'retire.json':
    scan_type = 'Retire.js Scan'

# Define the Bearer token
headers = {
    'Authorization': 'Token c1714a96aa0c3447bebbde2c182c6a355bf15dd1'
}

url = 'https://demo.defectdojo.org/api/v2/import-scan/'

data = {
    'active': True,
    'verified': True,
    'scan_type': scan_type,
    'minimum_severity': 'Low',
    'engagement': 31
} 
# let's send the file, rb = open the file and read in binary
files = {
    'file': open(file_name, 'rb')
}

# Lets send this file to the api

response = requests.post(url, headers=headers, data=data, files=files)

if response.status_code == 201:
    print ('Scan results imported successfully')

else:
    print (f'Failed to import Scan results: {response.content}')


